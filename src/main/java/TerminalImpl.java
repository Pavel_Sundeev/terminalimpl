import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TerminalImpl implements Terminal{

    private final TerminalServer terminalServer;
    private final PinValidator pinValidator;
    private final FrodMonitor frodMonitor;
    private final String id;
    private Account account;
    private boolean validPin = false;
    private int i;  //счетчик

    public TerminalImpl(TerminalServer terminalServer, PinValidator pinValidator,
                        FrodMonitor frodMonitor, String id) throws AccountException {
        this.terminalServer = terminalServer;
        this.pinValidator = pinValidator;
        this.frodMonitor = frodMonitor;
        this.id = id;
        account = terminalServer.getAccountId(id);
    }

    @Override
    public void loginToAccount(String pinCode, TerminalServer terminalServer, Account account) throws PinException {
        Date date = new Date();
        long secondDate = date.getTime() + (5 * 1000);
        try{
            if((new Date()).getTime() < secondDate){
                System.out.println("Аккаунт еще заблокирован, разблокируется через " +
                        (secondDate - (new Date().getTime() )));
            }else{
                PinValidator.checkIsPinCode(account, pinCode, terminalServer);
                validPin = true;
                i = 0;
            }
        }catch(PinException e){
            System.out.println(PinException.MESSAGE_FOR_CLIENT);
            i++;
            if(i > 3){
                System.out.println("Аккаунт заблокирован, время разблокировки " + date.getTime() + " + 5 сек");
            }
        }
    }

    @Override
    public void transferBetweenYourAccounts(int sum, String idDifferent,
                                            String pinCode, TerminalServer terminalServer)
            throws PinException, AmountException, AccountException {
        if(validPin == true){
            FrodMonitor.checkIsAmount(account, sum);
            terminalServer.setMapAccountValueMinus(this.account, sum);
            terminalServer.setMapAccountValuePlus(terminalServer.getAccountId(idDifferent), sum);
        }
        else{
            throw new PinException(PinException.MESSAGE_FOR_CLIENT);
        }
    }

    @Override
    public void transferToDifferentAccounts(String idDifferent, TerminalServer terminalServer, int sum,
                                            String pinCode)
            throws PinException, AmountException, AccountException {
        if(validPin == true){
            FrodMonitor.checkIsAmount(account, sum);
            terminalServer.setMapAccountValueMinus(this.account , sum);
            terminalServer.setMapAccountValuePlus(terminalServer.getAccountId(idDifferent), sum);
        }
        else{
            throw new PinException(PinException.MESSAGE_FOR_CLIENT);
        }
    }


}
