import java.util.Scanner;

public class PinValidator {

    private Account account;

    public static void checkIsPinCode(Account account, String pincode, TerminalServer terminalServer) throws PinException {
        if(terminalServer.getPinCode(account) == pincode){
            System.out.println("Вы успешно вошли в систему!");
        }
        else{
            throw new PinException(PinException.MESSAGE_FOR_CLIENT);
            }
        }
    }
