import java.util.Map;

public class TerminalServer {

    public static Map<Account, String> mapAccount;

    public TerminalServer(Map<Account, String> mapAccount){
        this.mapAccount = mapAccount;
    }

    public int getBalanceAccount(Account account){
        return account.getAmount();
    }

    public String getPinCode(Account account){
        return mapAccount.get(account);
    }

    public Account getAccountId(String id) throws AccountException {
        for (Account account : mapAccount.keySet()){
            if(account.getId() == id){
                return account;
            }
        }
        throw new AccountException(AccountException.MESSAGE_FOR_CLIENT);
    }

    public void setPinCode(Account account, String pin){
        String str = mapAccount.get(account);
        pin = str;
        str = mapAccount.get(account);
    }

    public String getMapAccountValue(Account account){
        return mapAccount.get(account);
    }

    public void setMapAccountValueMinus(Account account, int sum){
        account.setAmount(account.getAmount() - sum);
    }

    public void setMapAccountValuePlus(Account account, int sum){
        account.setAmount(account.getAmount() + sum);
    }
}



