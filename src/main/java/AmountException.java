public class AmountException extends Exception{

    public static final String MESSAGE_FOR_CLIENT = "Сумма перевода больше, чем остаток на карте или сумма " +
            "не кратна 100! ";

    public AmountException(String message) {
        super(message);
    }
}
