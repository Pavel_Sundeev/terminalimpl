public class PinException extends Exception{

    public static final String MESSAGE_FOR_CLIENT = "Вы ввели неправильный пароль или не ввели его!";

    public PinException(String message) {
        super(message);
    }
}
