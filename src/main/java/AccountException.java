public class AccountException extends Exception{

    public static final String MESSAGE_FOR_CLIENT = "Такого аккаунта не существует!";

    public AccountException(String message) {
        super(message);
    }
}
