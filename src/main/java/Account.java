public class Account {

    public static String id;
    public int amount;

    public Account(String id, int amount, String pinCode){
        this.id = id;
        this.amount = amount;
        TerminalServer.mapAccount.put(this, pinCode);
    }

    public void setId(String id){
        this.id = id;
    }

    public static String getId(){
        return id;
    }

    public int getAmount(){
        return amount;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    @Override
    public String toString() {
        return new StringBuilder("Id счета ")
                .append(id)
                .append(", ")
                .append("Баланс ")
                .append(amount).toString();
    }
}
