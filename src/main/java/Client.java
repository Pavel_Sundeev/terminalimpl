import java.util.List;

public class Client {

    List<Account> listAccount;
    public String name;
    public String lastName;

    public Client(List<Account> listAccount){
        this.listAccount = listAccount;
    }

    public Account getListAccount(int index){
        return listAccount.get(index);
    }

    public void setListAccount(int index, Account account){
        listAccount.set(index, account);
    }

    public int getSizeList(){
        return listAccount.size();
    }

    public void addListAccount(int index, Account account){
        listAccount.add(index, account);
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getLastName(){
        return this.lastName = lastName;
    }

    public void setLastName(String lastName){
        this.lastName = lastName;
    }
}
