public interface Terminal {

    void transferBetweenYourAccounts(int sum, String idDifferent,
                                     String pinCode, TerminalServer terminalServer)
            throws PinException, AmountException, AccountException;

    void transferToDifferentAccounts(String idDifferent, TerminalServer terminalServer, int sum,
                                     String pinCode)
            throws PinException, AmountException, AccountException;

    void loginToAccount(String pinCode, TerminalServer terminalServer, Account account) throws PinException;
}
