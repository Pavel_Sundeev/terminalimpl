public class FrodMonitor{

    public static void checkIsAmount(Account account, int sum /*String id*/) throws AmountException {
        //account.getId();
        if(account.getAmount() >= sum && sum % 100 == 0){
            System.out.println("Введена корректная сумма!");
        }else{
            throw new AmountException(AmountException.MESSAGE_FOR_CLIENT);
        }
    }
}
